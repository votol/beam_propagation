#include <chrono>
#include <iostream>
#include "imag.hpp"

double w_in=2.0;
double w_obj=0.4;
double F=15.0;
double k=1e5;

complex<double> in_distr(double x,double y)
    {
    complex<double> res;
    res=complex<double>(exp(-(x*x+y*y)/(2.0*w_in*w_in)));
    return res;
    }


int main(int argc,char** argv)
	{
	std::chrono::high_resolution_clock::time_point t1 = std::chrono::high_resolution_clock::now();
    std::chrono::high_resolution_clock::time_point t2;
    double a_img=F/(k*w_obj);
    field fin(1024,-5.0*w_in,5.0*w_in,1024,-5.0*w_in,5.0*w_in,-16);
    field fout(1024,-5.0*a_img,5.0*a_img,1024,-5.0*a_img,5.0*a_img,-16);
    fin.set(in_distr);
    fout.fourier(fin,k/F);
    t2=std::chrono::high_resolution_clock::now();
    std::cout<<std::chrono::duration_cast<std::chrono::microseconds>(t2-t1).count()<<std::endl;
    return 1;
	}
