#include "imag.hpp"
#include <iostream>

field::field(unsigned int nX, double maX, double miX, unsigned int nY, double maY, double miY, int type=-1):
           numX(nX),numY(nY),Xmin(miX),Xmax(maX),Ymin(miY),Ymax(maY),calc_type(type)
    {
    matr=std::shared_ptr<complex<double> >(new complex<double>[numX*numY]);
    dx=(Xmax-Xmin)/double(numX);
    dy=(Ymax-Ymin)/double(numY);
    }
    
field::~field()
    {
    
    }


void field::set(const complex<double> *in)
    {
    for (unsigned int j=0;j<numX*numY;j++)
        {
        matr.get()[j]=in[j];
        }
    }

void field::set_from_func_one_thread(distr in,unsigned int first,unsigned int count)
    {
    double x,y;
    y=Ymin+dy*double(first);
    
    for(unsigned int periy=first;periy<count;periy++)
        {
        x=Xmin;
        for(unsigned int perix=0;perix<numX;perix++)
            {
            matr.get()[periy*numX+perix]=in(x,y);
            x+=dx;
            }
        y+=dy;
        }
    }


void field::set(distr in)
    {
    std::thread *th=new std::thread[abs(calc_type)];
    for(unsigned int peri=0;peri<abs(calc_type);peri++)
        {
        th[peri]=std::thread(&field::set_from_func_one_thread,this,in,peri*numY/abs(calc_type),numY/abs(calc_type));
        }
    for(unsigned int peri=0;peri<abs(calc_type);peri++)
        th[peri].join();
    delete []th;
    }

void field::make_small_transform(field *in,unsigned int xi,unsigned int yi,double alpha,small_matrs &tmp_nums)
    {
    }

void field::make_x_transform_one_thread(field *in,unsigned int xi,unsigned int yi,unsigned int col,double alpha)
    {
    }

void field::make_x_transform(field &in,double alpha)
    {
    unsigned int xi=0;
    unsigned int yi=0;
    unsigned int dcol=(numX/16)*(in.numY/16)/abs(calc_type);
    std::thread *th=new std::thread[abs(calc_type)];
    for(unsigned int peri=0;peri<abs(calc_type);peri++)
        {
        th[peri]=std::thread(&field::make_x_transform_one_thread,this,&in,xi,yi,dcol,alpha);
        yi+=(dcol+xi)/(in.numX/16);
        xi=(dcol+xi)%(in.numX/16);
        }
    
    for(unsigned int peri=0;peri<abs(calc_type);peri++)
        th[peri].join();
    delete []th;
    }

void field::fourier(field &in,double alpha)
    {
    matr_tmp.reset(new complex<double>[numX*in.numY]);
    shift_d=exp(complex<double>(0,1)*dx);
    in.shift_d=exp(complex<double>(0,1)*alpha*dx);
    make_x_transform(in,alpha);
    matr_tmp.reset();
    }
