#ifndef __imag_hpp_
#define __imag_hpp_
#include <functional>
#include <thread>
#include <math.h>
#include <memory>
#include "complex.h"



class field
    {
    public:
        using distr=std::function<complex<double> (double,double)>;
    private:    
        unsigned int numX;
        unsigned int numY;
        double Xmin;
        double Xmax;
        double Ymin;
        double Ymax;
        
        double dx;
        double dy;
        
        int calc_type;
    
    
        std::shared_ptr<complex<double> > matr;
    
        void set_from_func_one_thread(distr,unsigned int ,unsigned int );
    
        /*for furie transform*/
        struct small_matrs
            {
            std::shared_ptr<complex<double> > in;
            std::shared_ptr<complex<double> > out;
            std::shared_ptr<complex<double> > exps;
            };
        
        complex<double> shift_d;
        complex<double> shift_16d;
        std::shared_ptr<complex<double> > matr_tmp;
        
        /*x furie transform*/
        
        /*gets coords in elements*/
        void make_small_transform(field *,unsigned int,unsigned int,double,small_matrs &);
        /*gets coords in grid of 16x16 blocks*/
        void make_x_transform_one_thread(field *,unsigned int,unsigned int,unsigned int,double);
        void make_x_transform(field &,double);
        
    
    public:
        
        
        field(unsigned int , double , double , unsigned int , double , double , int );
        ~field();
        void set(const complex<double> *);
        void set(distr);
        void fourier(field &,double);
    };

#endif /*__imag_hpp_*/
